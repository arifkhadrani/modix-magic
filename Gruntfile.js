module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
			dist: {
				files: {
					'web/assets/css/main.min.css': [
						'bower_components/bootstrap/less/bootstrap.less'
				  ]
				},
				options: {
				  compress: true,
				  // LESS source map
				  // To enable, set sourceMap to true and update sourceMapRootpath based on your install
				  sourceMap: false,
				  sourceMapFilename: 'web/assets/css/main.min.css.map',
				  sourceMapRootpath: '/web/'
				}
			}
		},
		uglify: {
			compress: {
				files: {
					'web/assets/js/main.js' : ['bower_components/bootstrap/js/*.js']
				}
			}
		},
		assemble: {
            options: {
				partials: ['src/modules/**/*.hbs'],
                layout: "src/layouts/default.hbs",
				data : "src/data/*.json" ,
                flatten: true
            },
            pages: {
                files: {
                    'web/': ['src/pages/*.hbs']
                }
            }
        },
		clean: {
			all: ['web/*.html', 'web/assets/css/*.css', 'web/assets/js/*.js']
		}
    });
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('assemble');
    grunt.registerTask('default', ['clean','less','uglify','assemble']);
};